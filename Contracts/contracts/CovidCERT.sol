// SPDX-License-Identifier: MIT
pragma solidity ^0.6.0;

import "OpenZeppelin/openzeppelin-contracts@3.4.0/contracts/token/ERC721/ERC721.sol";
import "OpenZeppelin/openzeppelin-contracts@3.4.0/contracts/token/ERC721/ERC721Burnable.sol";
import "OpenZeppelin/openzeppelin-contracts@3.4.0/contracts/access/AccessControl.sol";

contract CovidCERT is ERC721, ERC721Burnable, AccessControl {

    event Mint(address indexed to, uint256 indexed tokenId, bytes32 indexed tokenHash);

    bytes32 public constant MINTER_ROLE = keccak256("MINTER_ROLE");

    constructor() ERC721("CovidCERT", "C19") public {
        _setupRole(MINTER_ROLE, _msgSender());
    }

    function mint(address to, uint256 tokenId, bytes32 tokenHash) external {
        require(hasRole(MINTER_ROLE, _msgSender()), "CovidCERT: Caller is not a minter");
        _safeMint(to, tokenId);
        emit Mint(to, tokenId, tokenHash);
    }

}