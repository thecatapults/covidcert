from brownie import CovidCERT

def main():
    deployed_contract = CovidCERT.at("0x114A107C1931de1d5023594B14fc19d077FC4dfD")
    CovidCERT.publish_source(deployed_contract)
