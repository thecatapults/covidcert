from brownie import CovidCERT, accounts

def main():
    deployer = accounts.load('deployer')
    CovidCERT.deploy({'from': deployer}, publish_source=True)
