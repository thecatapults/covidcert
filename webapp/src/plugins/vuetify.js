import Vue from 'vue';
import Vuetify from 'vuetify';
import 'vuetify/dist/vuetify.min.css';

Vue.use(Vuetify);

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#1a2d64',
        secondary: '#83bae1',
        anchor: '#1a2d64',
      },
    },
  },
});
