import Vue from 'vue'
import App from './App.vue'
import vuetify from './plugins/vuetify';
import 'roboto-fontface/css/roboto/roboto-fontface.css'
import '@mdi/font/css/materialdesignicons.css'
import router from './router'
import firebase from 'firebase'
import Config from './config'

const app = firebase.initializeApp(Config.firebaseConfig);
window.firebase = app

Vue.config.productionTip = false

app.auth().onAuthStateChanged(()=>{
  new Vue({
    vuetify,
    router,
    render: h => h(App)
  }).$mount('#app')
})
