import TokenJSON from './CovidCERT.json'

const contractAddress = {
    'goerli': '0x7D4c5DfFf892b8EC0A6EC72ED1F2D09037875727',
    'kovan': '0x48536Ce5f6fcf98242eb2283289541F94dA24982'
}

const network = 'kovan'
const tokenAddress = contractAddress[network]

export default {
    firebaseConfig: {
        apiKey: "AIzaSyCGpu3nIXwOpJNIsqI0hjGcNyFU9cbz-WI",
        authDomain: "covicert.firebaseapp.com",
        projectId: "covicert",
        storageBucket: "covicert.appspot.com",
        messagingSenderId: "311442564079",
        appId: "1:311442564079:web:1f21001ef3be61c126cc1a",
        measurementId: "G-07J1GEQY1G"
    },
    blockchain: {
        network,
        tokenAddress,
        tokenABI: TokenJSON['abi'],
        infuraId: '2dcd5b142dcc43788af05f58d80a1842',
        adminMnemonic: 'clutch front marble elephant usual pupil planet shiver shoe angry whip hole',
    }
}